import React from "react";
import { Navbar, Nav } from "react-bootstrap";

class Navigation extends React.Component {
	render() {
		return (
			<Navbar bg="dark" variant="dark">
				<Navbar.Brand href={"/"}>Rick&Morty</Navbar.Brand>
				<Nav className="mr-auto">
					<Nav.Link href={"/"}>Characters</Nav.Link>
					<Nav.Link href={"/locations"}>Locations</Nav.Link>
				</Nav>
			</Navbar>
		);
	}
}

export default Navigation;
