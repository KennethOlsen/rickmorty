import React from "react";

const LocationCard = props => {
	return (
		<div className="col-xs-12 col-sm-6 col-md-4">
			<div className="card LocationCard">
				<div className="card-body">
					<h5 className="card-title">{props.name}</h5>
					<b>Id: </b> {props.id} <br />
					<b>Name: </b> {props.name} <br />
					<b>Type: </b> {props.type} <br />
					<b>Dimension: </b> {props.dimension} <br />
					<br />
				</div>
			</div>
		</div>
	);
};

export default LocationCard;
