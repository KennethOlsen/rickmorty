import React, { Component } from "react";
import { Form } from "react-bootstrap";
import _ from "lodash";

class SearchComponent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search: " "
		};

		this.handleSearchChange = _.debounce(this.handleSearchChange.bind(this), 300);
	}

	handleSearchChange(input) {
		if (this.props.onClick) {
			this.props.onClick(input);
		}
	}

	render() {
		return (
			<Form inline>
				<div type="text" className="mr-sm-2">
					<div>
						<input
							placeholder="Search character"
							onChange={e => this.handleSearchChange(e.target.value)}
						/>
					</div>
				</div>
			</Form>
		);
	}
}

export default SearchComponent;
