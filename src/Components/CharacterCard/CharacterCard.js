import React from "react";

import { Link } from "react-router-dom";

const CharacterCard = props => {
	const linkButton = props.showLink ? (
		<Link to={{ pathname: "/character/" + props.id }} className="btn btn-primary">
			View
		</Link>
	) : null;

	return (
		<div className="col-xs-12 col-sm-6 col-md-4">
			<div className="card CharacterCard">
				<img src={props.image} alt={props.name} className="card-img-top" />
				<div className="card-body">
					<h5 className="card-title">{props.name}</h5>
					<b>Species: </b> {props.species} <br />
					<b>Status: </b> {props.status} <br />
					<b>Gender: </b> {props.gender} <br />
					<b>Location: </b> {props.location} <br />
					<b>Place of origin: </b> {props.origin} <br />
					{linkButton}
					<br />
				</div>
			</div>
		</div>
	);
};

export default CharacterCard;
