import React from "react";

import { Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import CardsPage from "./containers/CharacterCards/CardsPage";
import CardPage from "./containers/CharacterCards/CardPage";
import LocationsPage from "./containers/LocationsPage";

import Navigation from "./Navbar";

import "./App.css";

class App extends React.Component {
	render() {
		return (
			<div>
				<Navigation />
				<Switch>
					<Route exact path="/" component={CardsPage} />
					<Route path="/character/:id" component={CardPage} />
					<Route path="/locations" component={LocationsPage} />
				</Switch>
			</div>
		);
	}
}

export default App;
