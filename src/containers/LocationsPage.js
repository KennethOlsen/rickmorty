import React from "react";
import LocationCard from "../Components/LocationCard/LocationCard";

class LocationsPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			locations: []
		};
	}

	componentDidMount() {
		this.getLocationData();
	}

	getLocationData = () => {
		fetch("https://rickandmortyapi.com/api/location/")
			.then(response => response.json())
			.then(data => {
				console.log(data);
				this.setState({ locations: data.results });
			})
			.catch(error => console.log(error));
	};

	render() {
		let locationCard = null;
		if (this.state.locations.length > 0) {
			locationCard = this.state.locations.map(
				location =>
					(locationCard = (
						<LocationCard
							key={location.id}
							id={location.id}
							name={location.name}
							type={location.type}
							dimension={location.dimension}
						/>
					))
			);
		} else {
			locationCard = <p>Loading locations!</p>;
		}

		return (
			<React.Fragment>
				<h4>Locations</h4>
				<br />
				{locationCard}
			</React.Fragment>
		);
	}
}
export default LocationsPage;
