import React from "react";
import CharacterCard from "../../Components/CharacterCard/CharacterCard";
import SearchComponent from "../../Components/SearchComponent";

class CardsPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			rickMorty: []
		};

		this.getData = this.getData.bind(this);
	}

	componentDidMount() {
		this.getData(" ");
	}

	getData(search) {
		console.log("Getting some data");
		fetch("https://rickandmortyapi.com/api/character/?name=" + search)
			.then(response => response.json())
			.then(data => {
				this.setState({ rickMorty: data.results });
			})
			.catch(error => console.log(error));
	}

	render() {
		let characters = null;

		if (this.state.rickMorty.length > 0) {
			characters = this.state.rickMorty.map(character => (
				<CharacterCard
					key={character.id}
					id={character.id}
					image={character.image}
					name={character.name}
					species={character.species}
					status={character.status}
					gender={character.gender}
					location={character.location.name}
					origin={character.origin.name}
					showLink={true}
				/>
			));
		} else {
			characters = <p>Loading...</p>;
		}

		return (
			<React.Fragment>
				<SearchComponent onClick={this.getData} />
				<h4>Characters</h4>
				<br />
				<div className="row">{characters}</div>
			</React.Fragment>
		);
	}
}
export default CardsPage;
