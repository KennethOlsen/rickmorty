import React from "react";
import CharacterCard from "../../Components/CharacterCard/CharacterCard";

class CardPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			character: {},
			id: props.match.params.id
		};
	}

	componentDidMount() {
		this.getCharacterData();
	}

	getCharacterData = () => {
		fetch("https://rickandmortyapi.com/api/character/" + this.state.id)
			.then(response => response.json())
			.then(data => {
				this.setState({ character: data });
			})
			.catch(error => console.log(error));
	};

	render() {
		let characterCard = null;
		if (this.state.character.id) {
			characterCard = (
				<CharacterCard
					key={this.state.character.id}
					id={this.state.character.id}
					image={this.state.character.image}
					name={this.state.character.name}
					species={this.state.character.species}
					status={this.state.character.status}
					gender={this.state.character.gender}
					location={this.state.character.location.name}
					origin={this.state.character.origin.name}
				/>
			);
		} else {
			characterCard = <p>Something crashed, yo!</p>;
		}

		return <React.Fragment>{characterCard}</React.Fragment>;
	}
}
export default CardPage;
